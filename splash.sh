#!/usr/bin/haserl
<%

# chores path
chores="/www/chores/chores.sh"
# redirect to original server address for jQuery ajax cross domain request
[ $SERVER_ADDR != $HTTP_HOST ] && echo -e "Status: 302 Found\nLocation: http://$SERVER_ADDR$SCRIPT_FILENAME" | sed -e 's/\/www//g'

echo "Content-Type: text/html; charset=utf-8"
echo

getWorkerMAC()
{
    # get worker and MAC
    deviMAC=$(sh $chores)
    # this makes deviMAC works as an array
    set $deviMAC > /dev/null
    
    # worker name is the 1st element
    worker=$(echo $1 | sed -e 's/"//g')

    # remove it from buffer
    shift
    
    # collect all MACs left in buffer
    deviMAC=$@
}

assignVar()
{
    # input $1 as name of worker
    API_URL="https://kingofstars001.000webhostapp.com/"
    
    # time to wait in splash screen (to do chores) then unban
    min_do_chores=5

    message="['MORNING', '*', 'WISH YOU', 'the BEST day', 'EVER!']"
    # replace * with worker name
    message=$(echo "$message" | awk -v w="$1" '{gsub("*", w, $0); printf $0}')
}

###MAIN starts here
# call getWorkerMAC to get worker of this day and his MACs
getWorkerMAC
# assign variable
assignVar $worker
# this makes deviMAC works as an array
set $deviMAC > /dev/null

# with every mac of this worker
while [ $# -gt 0 ]; do
    # get that MAC
    workerMAC=$(echo $1 | sed -e 's/"//g')

    # skip this mac if it is empty
    [ -z $workerMAC ] && shift && continue

    # get an ip assigned to that MAC
    workerIP=$(awk -v mac="$workerMAC" '(tolower(mac) ~ $4){printf $1}' /proc/net/arp)
    
    # check whether this device is banned
    isbanned=$(sh $chores "isbanned" $workerMAC)

    # if visitor is the worker and is banned
    if [ $REMOTE_ADDR = $workerIP ] && [ $isbanned -eq 1 ]; then
        # unban worker when splash screen showed up
        if [ $POST_swept = "done" ]; then
            sh $chores "unban"
            # unban only one time for all worker's MACs and show welcome screen
            break
        else
            # show splash screen
%>

<html><meta name="viewport" content="width=device-width, initial-scale=1.0"><head><title>603 Reminder....</title><style type="text/css">.ml5{position:absolute;font-weight:300;font-size:5vw;color:#402d2d;top:50%;transform:translate(0,-50%)}.ml5 .text-wrapper{position:relative;display:inline-block;padding-top:.1em;padding-right:.05em;padding-bottom:.15em;line-height:1em;left:50%;transform:translate(-25%,-50%)}.ml5 .line{position:absolute;left:0;top:0;bottom:0;margin:auto;height:3px;width:100%;background-color:#402d2d;transform-origin:.5 0}.ml5 .ampersand{font-family:Baskerville,"EB Garamond",serif;font-style:italic;font-weight:400;margin-right:-0.1em;margin-left:-0.1em}.ml5 .letters{display:inline-block;opacity:0}.ml5 .letters-left,.ml5,.letters-right{font-family:Arial,Calibri,sans-serif}</style><script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script></head><body style="background-color:#f3e9c6"><form id="myForm" method="POST"><input type="hidden" name="swept" Value="done" /></form><h1 class="ml5"><span class="text-wrapper"><span class="line line1"></span><span class="letters letters-left">DO CHORES</span> <span class="letters ampersand">&amp;</span> <span id='cd' class="letters letters-right"></span><span class="line line2"></span></span></h1><script>/*<![CDATA[*/var minutes=<% echo -n $min_do_chores %>;var target_date=new Date().getTime()+((minutes*60)*1000);var time_limit=((minutes*60)*1000);setTimeout(function(){document.getElementById("myForm").submit()},time_limit);var days,hours,minutes,seconds;var countdown=document.getElementById("cd");getCountdown();setInterval(function(){getCountdown()},1000);function getCountdown(){var c=new Date().getTime();var d=(target_date-c)/1000;if(d>=0){days=pad(parseInt(d/86400));d=d%86400;hours=pad(parseInt(d/3600));d=d%3600;minutes=pad(parseInt(d/60));seconds=pad(parseInt(d%60));countdown.innerHTML="<span>"+minutes+":</span><span>"+seconds+" LEFT</span>"}}function pad(b){return(b<10?"0":"")+b}/*]]>*/</script><script>anime.timeline({loop:true}).add({targets:".ml5 .line",opacity:[0.5,1],scaleX:[0,1],easing:"easeInOutExpo",duration:700}).add({targets:".ml5 .line",duration:600,easing:"easeOutExpo",translateY:function(h,e,f){var g=-0.625+0.625*2*e;return g+"em"}}).add({targets:".ml5 .ampersand",opacity:[0,1],scaleY:[0.5,1],easing:"easeOutExpo",duration:600,offset:"-=600"}).add({targets:".ml5 .letters-left",opacity:[0,1],translateX:["0.5em",0],easing:"easeOutExpo",duration:600,offset:"-=300"}).add({targets:".ml5 .letters-right",opacity:[0,1],translateX:["-0.5em",0],easing:"easeOutExpo",duration:600,offset:"-=600"}).add({targets:".ml5",opacity:0,duration:1000,easing:"easeOutExpo",delay:1000});</script></body></html>

<%            
            #exit because we don't want to show welcome screen here
            exit
        fi
    fi
    
    # remove this mac to process next one
    shift
done

# show welcome screen
%>

<!DOCTYPE html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8" />
<title>🚀🚀🚀</title>
<link href=lib/imageviewer.css rel=stylesheet type=text/css />
<script src=https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js></script>
<script src=https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js></script>
<script src=lib/imageviewer.min.js></script>
<style>body{background:#000}#image-gallery{width:100%;background:#000;display:none}#image-gallery .image-container{position:absolute;top:0;left:0;right:0;bottom:25px}#image-gallery .prev,#image-gallery .next{position:absolute;height:32px;margin-top:-66px;top:50%}#image-gallery .prev{left:20px}#image-gallery .next{right:20px;cursor:pointer}#image-gallery .footer-info{position:absolute;height:25px;width:100%;left:0;bottom:0;line-height:25px;font-size:24px;text-align:center;color:white;border-top:1px solid #fff}#msg{font-weight:900;font-size:2.5em}#msg .letters{position:absolute;margin:auto;top:0;right:150px;opacity:0;color:white;z-index:1}</style>
</head>
<body scroll=no style=overflow:hidden>
<div id=msg></div>
<div id=image-gallery>
<div class=image-container></div>
<img src=" data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDUxMiA1MTIiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48cGF0aCBkPSJNMzgyLjUsNTEyYy0zLjcsMC03LjQtMS4zLTEwLjMtNC4xTDExOS4yLDI3Mi41Yy0zLTIuOC00LjgtNi44LTQuOC0xMC45Yy0wLjEtNC4yLDEuNi04LjIsNC42LTExLjEgIEwzNzEuOSw0LjNjNi01LjgsMTUuNi01LjcsMjEuNSwwLjNjNS44LDYsNS43LDE1LjYtMC4zLDIxLjVMMTUxLjYsMjYxLjJsMjQxLjIsMjI0LjVjNi4xLDUuNyw2LjUsMTUuMywwLjgsMjEuNSAgQzM5MC42LDUxMC40LDM4Ni41LDUxMiwzODIuNSw1MTJ6IiBmaWxsPSIjNkE2RTdDIi8+PC9zdmc+" class=prev />
<img src=" data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDUxMiA1MTIiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48cGF0aCBkPSJNMTI5LjYsNTEyYy00LjEsMC04LjEtMS42LTExLjEtNC44Yy01LjctNi4xLTUuNC0xNS43LDAuOC0yMS41bDI0MS4yLTIyNC41TDExOSwyNi4xICBjLTYtNS45LTYuMS0xNS41LTAuMy0yMS41YzUuOS02LDE1LjUtNi4xLDIxLjUtMC4zTDM5MywyNTAuNWMzLDIuOSw0LjcsNi45LDQuNiwxMS4xYy0wLjEsNC4yLTEuOCw4LjEtNC44LDEwLjlMMTM5LjksNTA3LjkgIEMxMzcsNTEwLjcsMTMzLjMsNTEyLDEyOS42LDUxMnoiIGZpbGw9IiM2QTZFN0MiLz48L3N2Zz4=" class=next />
<div class=footer-info>
<span class=current></span>/<span class=total></span>
</div>
</div>
<script type=text/javascript>var API="<% echo -n $API_URL %>";
var text_welcome=<% echo -n $message %>;var msg={};msg.opacityIn=[0,1];msg.scaleIn=[0.2,1];msg.scaleOut=2;msg.durationIn=800;msg.durationOut=600;msg.delay=500;var loop=anime.timeline({loop:true});function onJSONLoaded(imagesJSON){if(imagesJSON.success){$("#image-gallery").show();}
var curImageIdx=1,total=imagesJSON.total;var wrapper=$('#image-gallery'),curSpan=wrapper.find('.current');var viewer=ImageViewer(wrapper.find('.image-container'));wrapper.find('.total').html(total);function showImage(){var index=curImageIdx-1;viewer.load(imagesJSON.small[index],imagesJSON.large[index]);curSpan.html(curImageIdx);}
wrapper.find('.next').click(function(){curImageIdx++;if(curImageIdx>total)curImageIdx=1;showImage();});wrapper.find('.prev').click(function(){curImageIdx--;if(curImageIdx<1)curImageIdx=total;showImage();});showImage();}
for(let i=0;i<text_welcome.length;i++){$("#msg").append('<span class="letters letters-'+(i+1)+'">'+text_welcome[i]+'</span>');loop.add({targets:'#msg .letters-'+(i+1),opacity:msg.opacityIn,scale:msg.scaleIn,duration:msg.durationIn}).add({targets:'#msg .letters-'+(i+1),opacity:0,scale:msg.scaleOut,duration:msg.durationOut,easing:"easeInExpo",delay:msg.delay});}
$.getJSON(API).done(onJSONLoaded).fail(function(jqXHR,textStatus){console.log("Request failed: "+textStatus);});</script>
</body>
</html>
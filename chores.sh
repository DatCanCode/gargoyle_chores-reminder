#!/bin/sh

#usage: chores.sh [ban/unban/isbanned <mac>]

API_DOMAIN="cdnjs.cloudflare.com"

getWorkerMAC()
{
    case $(date +%u) in
        1)
            worker='"Vinh" "A0:88:69:BC:50:48" "74:04:2B:B1:91:3C" "E4:A7:C5:E2:8D:8B"'
            ;;
        2)
            worker='"Nhất" "20:10:7A:6C:B7:B6" "08:78:08:50:FA:6A"'
            ;;
        3)
            worker='"Công" "40:9B:CD:00:75:A7" "C4:9F:4C:91:9A:C3"'
            ;;
        4)
            worker='"Trưa" "18:CF:5E:67:87:BA" "04:B1:67:1D:7C:38"'
            ;;
        5)
            worker='"Thái" "34:41:5D:D5:2E:10" "E8:50:8B:32:32:3E"'
            ;;
        6)
            worker='"Quân" "20:16:D8:0F:57:73" "84:C7:EA:21:CA:71"'
            ;;
        7)
            worker='"Đạt" "DC:85:DE:94:16:05" "94:87:E0:63:3E:AE"'
            ;;
    esac
}

banMAC() {
    # check for existence of 2 args: MAC & except
    if [ $# -eq 2 ]; then
        deviMAC=$1
        except=$2
        
        # only ban if device is not banned
        isBanned $deviMAC
		if [ $? -eq 0 ]; then
            iptables -t nat -I PREROUTING -p tcp --dport 80 -m mac --mac-source $deviMAC -j DNAT --to-destination 192.168.1.1:8080
            iptables -t nat -I PREROUTING -p tcp --dport 443 -m mac --mac-source $deviMAC -j DNAT --to-destination 192.168.1.1:8081
            iptables -t nat -I PREROUTING -p tcp -d $except -m mac --mac-source $deviMAC -j ACCEPT
            iptables -I FORWARD -m mac --mac-source $deviMAC -j DROP
            iptables -I FORWARD -p tcp -d $except -m mac --mac-source $deviMAC -j ACCEPT
        fi
    fi
}

unBanMAC() {
    # check for existence of 2 args: MAC & except
    if [ $# -eq 2 ]; then
        deviMAC=$1
        except=$2
        
        # only unban if device is banned
        isBanned $deviMAC
		if [ $? -eq 1 ]; then
            iptables -t nat -D PREROUTING -p tcp --dport 80 -m mac --mac-source $deviMAC -j DNAT --to-destination 192.168.1.1:8080
            iptables -t nat -D PREROUTING -p tcp --dport 443 -m mac --mac-source $deviMAC -j DNAT --to-destination 192.168.1.1:8081
            iptables -t nat -D PREROUTING -p tcp -d $except -m mac --mac-source $deviMAC -j ACCEPT
            iptables -D FORWARD -m mac --mac-source $deviMAC -j DROP
            iptables -D FORWARD -p tcp -d $except -m mac --mac-source $deviMAC -j ACCEPT
        fi
    fi
}

isBanned() {
    if [ $# -eq 1 ]; then
        deviMAC=$1
        iptables -t nat -C PREROUTING -p tcp --dport 80 -m mac --mac-source $deviMAC -j DNAT --to-destination 192.168.1.1:8080 > /dev/null 2>&1
        return $(expr 1 - $?)
    fi
}

###MAIN starts here
getWorkerMAC

# print worker of today if this script is executed without any args
if [ $# -eq 0 ]; then
    echo $worker
elif [ $# -eq 1 ]; then
    command=$1
    # this makes $worker works as an array
    set $worker
    # remove first element (name)
    shift
    
    # ban/unban user
    if [ $command = "ban" ]; then
        # with every mac of this worker
        while [ $# -gt 0 ]; do
            # get that MAC
            workerMAC=$(echo $1 | sed -e 's/"//g')
            # ban MAC if it's not empty
            [ $workerMAC ] && banMAC $workerMAC $API_DOMAIN
            # get next mac
            shift
        done
    elif [ $command = "unban" ]; then 
        # with every mac of this worker
        while [ $# -gt 0 ]; do
            # get that MAC
            workerMAC=$(echo $1 | sed -e 's/"//g')
            # unban MAC if it's not empty
            [ $workerMAC ] && unBanMAC $workerMAC $API_DOMAIN
            # get next mac
            shift
        done
    fi
elif [ $# -eq 2 ]; then
    command=$1
    deviMAC=$2
    
    if [ $command = "isbanned" ]; then
        isBanned $deviMAC
        echo $?
    fi
fi
